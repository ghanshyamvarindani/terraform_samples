variable project {
  default = "learning-278802"
}
variable zone {
  default = "asia-southeast1-b"
}

variable region {
  default = "asia-southeast1"
}