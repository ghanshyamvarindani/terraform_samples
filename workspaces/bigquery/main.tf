

provider "google" {
  #   credentials = file("account.json")
  access_token = "<place your token here "gcloud auth application-default login">"
  #   project     = "learning-278802"
  project = var.project
  region  = var.region
  #   region      = "us-central1"
}

module bigquery_common {
  source  = "/Users/ghanshyamdasvarindani/Desktop/Learning/terraform/terraform_samples/modules/bigquery"
  project = var.project
  region  = var.region
}

