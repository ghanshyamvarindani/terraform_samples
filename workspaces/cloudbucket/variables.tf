variable project {
  default = "learning-278802"
}
variable zone {
  default = "us-central1-a"
}

variable region {
    default = "us-central1"
}