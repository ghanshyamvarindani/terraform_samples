resource "google_storage_bucket" "auto-expire" {
  name          = "terraform_bucket"
  location      = "US"
  force_destroy = true

  lifecycle_rule {
    condition {
      age = "3"
    }
    action {
      type = "Delete"
    }
  }
}