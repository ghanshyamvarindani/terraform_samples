variable terraform_table_config {
  default = {
    terraform_table = { version = "v1" }
  }
}

variable project {}
variable region {}


resource "google_bigquery_dataset" "dataset_terraform" {
  dataset_id                  = "terraform_bq"
  friendly_name               = "terraform bigquery dataset"
  description                 = "This is a test dataset for bq"
  default_table_expiration_ms = 3600000
  location                    = var.region
  project                     = var.project
  labels = {
    env = "default"
  }

  access {
    role          = "OWNER"
    user_by_email = "varindani.ghanshyam@gmail.com"
    # google_service_account.bqowner.account_id
  }

  access {
    role           = "READER"
    user_by_email = "gvarindani261982@gmail.com"
  }

  # resource "google_service_account" "bqowner" {
  #   account_id = "varindani.ghanshyam@gmail.com"
  # }
}



resource "google_bigquery_table" "terraform_tables" {
  dataset_id = google_bigquery_dataset.dataset_terraform.dataset_id

  for_each =     var.terraform_table_config
  table_id = each.key


  #   labels = {
  #     env = var.env
  #   }

  #   schema  = file("${path.module}/schemas/ground_ops_analytics/${each.key}_${(each.value).version}.json")
  schema  = file("/Users/ghanshyamdasvarindani/Desktop/Learning/terraform/terraform_samples/modules/bigquery/terraform_table.json")
  project = var.project

}